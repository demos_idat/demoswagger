package com.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Persona;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/personas")
@Api(value = "Servicio REST para personas")
public class DemoController {
	
	
	
	@ApiOperation("Rertorna una lista de personas")
	@GetMapping
	public List<Persona> listar(){ //http://localhost:8085/demo/personas
		List<Persona> lista = new ArrayList<>();
		lista.add(new Persona(1, "Leonel", "Messi"));
		lista.add(new Persona(2, "Paolo", "Gerrero"));
		lista.add(new Persona(3, "Cristian", "Cueva"));
		return lista;
	}

}
